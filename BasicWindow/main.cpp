#pragma comment(lib, "glew32.lib")
#pragma comment(lib, "opengl32.lib")

#include <Windows.h>

#include <GL\glew.h>
#include <GL\wglew.h>

#include <string>
#include <fstream>
#include <iostream>
using std::string;
using namespace std;

struct WindowStruct
{
	HINSTANCE hInstance;
	HWND hWnd;
};

struct Context {
	HGLRC hrc;
	HDC hdc;
};

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message) {
	case WM_CLOSE:
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

WindowStruct CreateCustomWindow(const string& title)
{
	WindowStruct window;
	window.hInstance = 0;
	window.hWnd = NULL;

	window.hInstance = GetModuleHandle(NULL);
	if (window.hInstance == 0) return window;

	WNDCLASS windowClass;
	windowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = (WNDPROC)WndProc;
	windowClass.cbClsExtra = 0;
	windowClass.cbWndExtra = 0;
	windowClass.hInstance = window.hInstance;
	windowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.hbrBackground = NULL;
	windowClass.lpszMenuName = NULL;
	windowClass.lpszClassName = title.c_str();

	if (!RegisterClass(&windowClass)) {
		window.hInstance = 0;
		window.hWnd = NULL;
		return window;
	}

	ifstream fin;
	fin.open("windows.config");
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}//if

	int height = 420;		//default values
	int width = 666;
	int x = 0;				//text file should have 100/100 startpos and 100 height/1000 width
	int y = 0;


	string word;
	while (!fin.eof())					//search for height & width. works in any order, so
	{										//both are done in the same loop
		fin >> word;
		if (word == "<width>")
			fin >> width;
		if (word == "<height>")
			fin >> height;
		if (fin.fail())
			break;
	}//while

	fin.close();
	fin.open("windows.config");
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}//if

	while (!fin.eof())					//search for start position. seperated into its own loop
	{										//to avoid errors regarding which order they are found in
		fin >> word;
		if (word == "<startpos>")
		{
			fin >> x;
			fin >> y;
		}//if
		if (fin.fail())
			break;
	}//while

	window.hWnd = CreateWindowEx(
		WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
		title.c_str(),
		title.c_str(),
		WS_OVERLAPPEDWINDOW,
		x, y, width, height,
		NULL,
		NULL,
		window.hInstance,
		NULL
		);

	if (window.hWnd == NULL) {
		window.hInstance = 0;
		window.hWnd = NULL;
	}

	return window;
}

void MessageLoop(const Context& context)
{
	bool timeToExit = false;
	MSG msg;
	while (!timeToExit) {
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
			if (msg.message == WM_QUIT) {
				timeToExit = true;
			}
			else {
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else {
			glViewport(0, 0, 500, 500);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
			SwapBuffers(context.hdc);
		}

	}
}

Context CreateOGLWindow(const WindowStruct& window)
{
	Context context;
	context.hdc = NULL;
	context.hrc = NULL;

	if (window.hWnd == NULL) return context;

	context.hdc = GetDC(window.hWnd);
	if (context.hdc == NULL) return context;

	PIXELFORMATDESCRIPTOR pfd;
	memset(&pfd, 0, sizeof(PIXELFORMATDESCRIPTOR));
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cDepthBits = 32;

	int pixelFormat = ChoosePixelFormat(context.hdc, &pfd);
	if (pixelFormat == 0) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	BOOL result = SetPixelFormat(context.hdc, pixelFormat, &pfd);
	if (!result) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	HGLRC tempOpenGLContext = wglCreateContext(context.hdc);

	wglMakeCurrent(context.hdc, tempOpenGLContext);

	if (glewInit() != GLEW_OK) {
		context.hdc = NULL;
		context.hrc = NULL;
		return context;
	}

	return context;
}

void ShowWindow(HWND hWnd)
{
	if (hWnd != NULL) {
		ShowWindow(hWnd, SW_SHOW);
		UpdateWindow(hWnd);
	}
}

void CleanUp(const WindowStruct& window, const Context& context)
{
	if (context.hdc != NULL) {
		wglMakeCurrent(context.hdc, 0);
		wglDeleteContext(context.hrc);
		ReleaseDC(window.hWnd, context.hdc);
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	string title = "Default title";		//default values in case of error
	GLclampf r = 0.7f;					//dark orange color
	GLclampf g = 0.3f;
	GLclampf b = 0.0f;					//color indicated in windows.config should be a dark green
	GLclampf a = 0.9f;

	ifstream fin;
	fin.open("windows.config"); 
	if (fin.fail()) 
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}//if

	string word;					//search for title
	while (!fin.eof())
	{
		fin >> word;
		if (word == "<title>")
			getline(fin, title);
		if (fin.fail())
			break;
	}
	fin.close();					//file is closed & reopened to start at the beginning for the next search
									//this eliminates errors regarding which order the title & color are in
	fin.open("windows.config");			//in the text file
	if (fin.fail())
	{
		cout << "\nThere was error opening the file." << endl;
		exit(1);
	}//if
	while (!fin.eof())					//search for color
	{
		fin >> word;
		if (word == "<color>")
		{
			fin >> r;
			fin >> g;
			fin >> b;
			fin >> a;
		}
		if (fin.fail())
			break;
	}//while

	WindowStruct window = CreateCustomWindow(title);
	if (window.hWnd != NULL) {
		Context context = CreateOGLWindow(window);
		if (context.hdc != NULL) {
			ShowWindow(window.hWnd);

			glClearColor(r, g, b, a);
			MessageLoop(context);
			CleanUp(window, context);
		}

	}

	return 0;
}
