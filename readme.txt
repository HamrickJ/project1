Formatting is done using the following tags:
<startpos>, <height>, <width>, <title>, and <color>

Each starts on a new line
They are followed by a space and then the value, such as
<title> video games are silly

<color> is followed by four float values, such as
<color> 0.1f 0.2f 0.3f 0.4f

